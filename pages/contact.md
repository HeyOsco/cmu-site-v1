---
layout              : page
title               : "Contact"
meta_title          : "Contact and use our contact form"
subheadline         : "Email is cringe, but everyone has one so here we are"
teaser              : "Got any questions or business inquiries?"
permalink           : "/contact/"
header:
   image_fullwidth  : "og/contact2.jpg"
---
Shoot us an email and we'll get back to you ASAP!

### Email
```
cmufaculty@tutanota.com
```
