---
layout              : page
title               : "About"
meta_title          : "About"
# subheadline         : "Clown Math University"
teaser              : "Welcome back to school!"
permalink           : "/about/"
header:
   image_fullwidth  : "og/about1.jpg"
---
We are building **CLOWN MATH** (*Cryptids Loving Our World Now, Masquerading as Tricky Humans*) - The idea is that we are creating a visual novel (or immersive story) designed for WebXR, so you can play the story on your phone, your computer, or even VR headset by just visiting a website! 

The story is that your are a new member of the faculty of what you thought was a normal academy, but turns out is a school for training the next generation of cryptids to fit into human society! 

The setting will obviously have a lot of allusions to the crypids on the faculty board, but a lot of the inspiration for scene design (which will be our focus as this is something you can only do in VR!) will be famous magic academies such as Hogwarts in the Harry Potter Series and  Clown Tower in the Fate/Stay Night series. 

## Team (Your Fearless Faculty)
{% for author in site.data.about %}
  <h3>{{ author.name }}</h3>
  <img src="{{ author.avi }}" alt="{{ author.name }}" /><br>
  <p>
    <b>Role</b>: {{ author.role }} <br/>
    <b>Favorite Anime</b>: {{ author.anime }} <br/>
    <b>Site</b>: <a href="{{ author.site }}">{{author.site}}</a><br/>
    <b>Random Fun Fact</b>: {{ author.funfact }} <br/>
  </p>
{% endfor %}