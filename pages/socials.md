---
layout              : page
title               : "Socials"
meta_title          : "Socials"
subheadline         : "Perceive Our Existences"
teaser              : "Or not, because we're cryptids lol"
permalink           : "/socials/"
header:
   image_fullwidth  : "og/socials1.jpg"
---
{% for social in site.data.socialmedia %}
  <p>
    <a href="{{ social.url }}">{{social.name}}</a><br/>
  </p>
{% endfor %}
