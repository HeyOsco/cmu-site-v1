---
permalink: /search/
layout: page
title: "Search"
sitemap: false
subheadline         : "Find what is lost in the hidden forest of CMU"
teaser              : "What are you looking for?"
header:
   image_fullwidth  : "og/search1.jpg"
---

{% include _google_search.html %}
