---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header:
  image_fullwidth: og/academy.jpg
widget1:
  title: "About"
  url: 'http://clown-math.university/about/'
  image: widgets/teacher3-302x182.jpg
  text: 'Welcome to CLOWN MATH! This is an interactive narrative experience where you will take the place of a new professor at a not-quite-so normal university, and quickly learn that this an institution for teaching cryptids on how to immerse themselves into human society!'
widget2:
  title: "Episodes"
  url: 'http://clown-math.university/episodes/season-0/'
  image: widgets/cellar-302x182.jpg
  text: 'Experience the story straight from your phone, computer, or virtual reality headset. No download required, you just need a web browser and internet connection! (But you probably already have those from being here, right?)'
widget3:
  title: "Newsletter"
  url: 'http://clown-math.university/newsletter/archive/'
  image: widgets/newspaper2-302x182.jpg
  text: 'Keep up to date with all things CLOWN MATH! New subscribers to the newsletter will receive a special limited-time welcome package.'
#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#
callforaction:
  url: https://newsletter.clown-math.university/
  text: Sign up for our newsletter! ›
  style: alert
permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---

<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>
